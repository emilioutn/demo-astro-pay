package com.example.demo.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.math.BigDecimal;
import java.util.Date;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WalletMovement {
    @Id
    private Long id;
    private Wallet wallet;
    private BigDecimal amount;
    private String type;
    private String description;
    private Date timestamp;
}
