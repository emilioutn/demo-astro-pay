package com.example.demo.dto.transaction;

import com.example.demo.domain.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WalletDto {
    private Long id;
    private User user;
    private String currency;
    private BigDecimal amount;
}
