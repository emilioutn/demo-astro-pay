package com.example.demo.dto.transaction;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MovementDto {
    private BigDecimal amount;
    private String currency;
    private String type;
    private String description;
}
