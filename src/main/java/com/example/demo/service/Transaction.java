package com.example.demo.service;

import com.example.demo.domain.Wallet;
import com.example.demo.domain.WalletMovement;
import com.example.demo.dto.transaction.MovementDto;
import com.example.demo.dto.transaction.WalletDto;
import com.example.demo.exceptions.WalletNotFound;
import com.example.demo.repository.WalletMovementRepository;
import com.example.demo.repository.WalletRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Slf4j
@Service
public class Transaction {

    @Autowired
    private WalletMovementRepository walletMovementRepository;

    @Autowired
    private WalletRepository walletRepository;

    public void cashIn(WalletDto walletDto, MovementDto movementDto) throws WalletNotFound {
        Wallet wallet = getWalletById(walletDto.getId());
        if (wallet != null) {
            try {
                wallet.setAmount(wallet.getAmount().add(movementDto.getAmount()));
                walletMovementRepository.save(createMovement(wallet, movementDto));
                walletRepository.save(wallet);
            } catch (Exception e) {
                // TODO - ADD IN TABLE OF TRANSACTIONS FAILED FOR TRY TO DO THE SAVE FUNCTION LATER
            }
        } else {
            throw new WalletNotFound();
        }
    }

    public void cashOut(WalletDto walletDto, MovementDto movementDto) throws WalletNotFound {
        Wallet wallet = getWalletById(walletDto.getId());
        if (wallet != null) {
            try {
                wallet.setAmount(wallet.getAmount().subtract(movementDto.getAmount()));
                walletMovementRepository.save(createMovement(wallet, movementDto));
                walletRepository.save(wallet);
            } catch (Exception e) {
                // TODO - ADD IN TABLE OF TRANSACTIONS FAILED FOR TRY TO DO THE SAVE FUNCTION LATER
            }
        } else {
            throw new WalletNotFound();
        }
    }

    public void transfer(WalletDto walletOriginDto, WalletDto walletDestDto, MovementDto movementDto)
            throws WalletNotFound {
        cashOut(walletOriginDto, movementDto);
        cashIn(walletDestDto, movementDto);
    }

    private WalletMovement createMovement(Wallet wallet, MovementDto movementDto) {
        return WalletMovement
                .builder()
                .wallet(wallet)
                .amount(movementDto.getAmount())
                .type(movementDto.getType())
                .description(movementDto.getDescription())
                .timestamp(new Date())
                .build();
    }

    private Wallet getWalletById(Long id) {
        return walletRepository.findById(id).orElse(null);
    }
}
