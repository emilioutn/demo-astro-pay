package com.example.demo.repository;

import com.example.demo.domain.WalletMovement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WalletMovementRepository extends JpaRepository<WalletMovement, Long> {
}
