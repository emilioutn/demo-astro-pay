# Exercise AstroPay

## Description:

### Ejercicio 1

Escribir un programa en Java que tenga los siguientes métodos que implementan las operaciones básicas de un banco: depósito, extracción y transferencia.
Los componentes de los objetos quedan a cargo del developer.

````````
void cashIn(ObjectAccount, ObjectAmount)
void cashOut(ObjectAccount, ObjectAmount)
void transfer(ObjectAccountOrigin, ObjectAccountDest, ObjectAmount)
````````

### Ejercicio 2

Obtener utilizando una consulta SQL el listado de users (nombre, apellido y documento) que tengan saldo mayor a 50.000 y que no tengan ningún movimiento de venta (description) en el último mes.


#### Tablas:

````````
APPX_user (id, first_name, last_name, document, country)
APPX_wallet (id, user_id, currency, amount)
APPX_wallet_movement (id, wallet_id, amount, type, description, timestamp)
````````

# Resolution of exercise 2

````````
SELECT user.NAME, user.LASTNAME, user.NU_DOC
FROM
APPX_wallet wallet
INNER JOIN APPX_wallet_movement movements ON wallet.id = movements.wallet_id
INNER JOIN APPX_user user ON wallet.user_id = user.id
WHERE movements.amount > 50000 AND movements.description <> 'venta'
AND EXTRACT(MONTH FROM movements.timestamp) = EXTRACT(MONTH FROM SYSDATE)
AND EXTRACT(YEAR FROM movements.timestamp) = EXTRACT(YEAR FROM SYSDATE);
````````


## Note:

This exercise was done keeping the ORACLE SQL syntax